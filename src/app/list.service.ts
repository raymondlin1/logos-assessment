import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
//import { Task } from './task';

@Injectable({
  providedIn: 'root'
})
export class ListService {
  constructor(private db: AngularFireDatabase) { }

  // fetch tasks from firebase db
  fetchTasks(): Observable<any[]>
  {
    return this.db.list('tasks').snapshotChanges()
      .pipe(
        map(actions => 
          actions.map(a => {
            let id = a.key;
            let values = a.payload.val();
            return { id, values };
          }))
      );
  }

  postTask(task): void
  {
    this.db.list('tasks').push(task);
  }

  deleteTask(id): void
  {
    this.db.list('tasks').remove(id);
  }

  updateTask(id, newTask): void
  {
    this.db.list('tasks').update(id, newTask);
  }
}
