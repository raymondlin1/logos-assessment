import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { ListService } from '../list.service';
import { ElementSchemaRegistry } from '@angular/compiler';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  taskList: any[];
  creatingNewTask: boolean;
  @ViewChild("taskBox") taskBox: ElementRef;

  constructor(private listService: ListService) { }

  ngOnInit(): void 
  {
    this.getTasks();
    this.creatingNewTask = false;
  }

  getTasks(): void
  {
    this.listService.fetchTasks().subscribe(res => {
      this.taskList = res;
    });
  }

  onCheckTaskChange(target): void
  {
    target.values.completed = !target.values.completed;
    let newTask = {
      completed: target.values.completed,
      task: target.values.task
    };
    this.listService.updateTask(target.id, newTask);
  }

  onNewTask()
  {
    this.creatingNewTask = true;
    setTimeout(() => { 
      this.taskBox.nativeElement.focus();
    },0); 
  }

  onNewTaskEnter(input: string, completed: boolean): void
  {
    this.creatingNewTask = false;
    let newTask = {
      completed: completed,
      task: input
    };

    this.taskList.push(newTask);
    this.listService.postTask(newTask);
  }

  onTaskDelete(target): void
  {
    this.listService.deleteTask(target.id);
    for(let i = 0; i < this.taskList.length; i++)
    {
      if(this.taskList[i] == target)
      {
        this.taskList.splice(i, 1);
        break;
      }
    }

    
  }
}
