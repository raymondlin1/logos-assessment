export interface Task
{
    id: number;
    completed: boolean;
    task: string;
}