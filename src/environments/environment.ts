// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

export const firebaseConfig = {
  apiKey: "AIzaSyDlT6UoDQsiUyU8YTUooOvRq0RSYcphItY",
  authDomain: "logos-assessment.firebaseapp.com",
  databaseURL: "https://logos-assessment.firebaseio.com",
  projectId: "logos-assessment",
  storageBucket: "logos-assessment.appspot.com",
  messagingSenderId: "638766022140",
  appId: "1:638766022140:web:d108238cc39fcffc468679",
  measurementId: "G-6LYTMBNWJN"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
